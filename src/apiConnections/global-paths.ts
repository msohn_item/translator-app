export const PATHS: { [key: string]: string } = {
  TEXT_LOCALIZATION_URL_DEV: process.env.VUE_APP_TEXT_LOCALIZATION_URL_DEV,
  TEXT_LOCALIZATION_URL_STAGE: process.env.VUE_APP_TEXT_LOCALIZATION_URL_STAGE,
  TEXT_LOCALIZATION_URL_PROD: process.env.VUE_APP_TEXT_LOCALIZATION_URL_PROD
};
