import axios from 'axios';
import { PATHS } from './global-paths';

const textLocalization = {
  textLocalizationUrl: '',
  setEnvironment(env: string) {
    this.textLocalizationUrl = PATHS[`TEXT_LOCALIZATION_URL_${env.toUpperCase()}`];
  },
  /**
   * Methode zur API Anfrage aller
   */
  async getAll(sourceLang: string, targetLang: string, excludeTranslated: boolean) {
    return await axios
      .get(
        `${this.textLocalizationUrl}/?lang=${targetLang}&source-lang=${sourceLang}&exclude-translated=${excludeTranslated}`
      )
      .then(response => response.data)
      .catch(error => error.response);
  },

  async getSelected(route: string[], sourceLang: string, targetLang: string) {
    const paths = new URLSearchParams();

    route.forEach(path => {
      paths.append('path', path);
    });

    return await axios
      .get(`${this.textLocalizationUrl}?lang=${targetLang}&source-lang=${sourceLang}`, {
        params: paths,
        headers: { accept: 'application/xml' }
      })
      .then(response => response.data)
      .catch(error => console.log(error.response));
  },
  async updateSubDoc(route: string, updatedObject: { [key: string]: string }) {
    console.log(updatedObject);
    return await axios
      .patch(`${this.textLocalizationUrl}/${route}`, updatedObject)
      .then(response => {
        return response;
      })
      .catch(error => error.response);
  },
  async addSubDoc(route: string, updatedObject: { [key: string]: string }) {
    return await axios
      .put(`${this.textLocalizationUrl}/${route}`, updatedObject)
      .then(response => {
        return response;
      })
      .catch(error => error.response);
  },
  async updateFromXML(xml: any) {
    return await axios
      .patch(`${this.textLocalizationUrl}`, xml, { headers: { 'content-type': 'application/xml' } })
      .then(response => {
        return response;
      })
      .catch(error => error.response);
  }
};

export default textLocalization;
