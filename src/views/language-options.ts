export const languageOptions = [
    {
      id: 'de',
      label: 'Deutsch'
    },
    {
      id: 'en',
      label: 'Englisch'
    },
    {
      id: 'it',
      label: 'Italienisch'
    },
    {
      id: 'fr',
      label: 'Französisch'
    },
    {
      id: 'es',
      label: 'Spanisch'
    },
    {
      id: 'zh',
      label: 'Chinesisch'
    },
    {
      id: 'ru',
      label: 'Russisch'
    },
    {
      id: 'pl',
      label: 'Polnisch'
    },
    {
      id: 'nl',
      label: 'Niederländisch'
    },
    {
      id: 'cs',
      label: 'Tschechisch'
    },
    {
      id: 'pt',
      label: 'Portugiesisch'
    },
    {
      id: 'sv',
      label: 'Schwedisch'
    },
    {
      id: 'tr',
      label: 'Türkisch'
    },
    {
      id: 'el',
      label: 'Griechisch'
    }
  ];
  