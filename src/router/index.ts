import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import TranslatorMainPage from '../views/TranslatorMainPage.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'TranslatorMainPage',
    component: TranslatorMainPage
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
