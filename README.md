# translator-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### env
VUE_APP_TEXT_LOCALIZATION_URL_DEV=http://localhost:3011
VUE_APP_TEXT_LOCALIZATION_URL_STAGE=http://localhost:3012
VUE_APP_TEXT_LOCALIZATION_URL_PROD=http://localhost:3013